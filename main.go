package main

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/prahlad.singh-coditas/booking-app.git/helper"
)

const conferenceTickets int = 50

var conferenceName = "Go Conference"
var remainingTickets int = 50
var bookings = make([]UserData, 0)

type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets int
}

var wg = sync.WaitGroup{}

func main() {

	greetUsers()

	//ask users for their name
	firstName, lastName, email, userTickets := getUserDetails()

	isValidName, isValidEmail, isValidTicketNumber := helper.GetValidDetails(firstName, lastName, email, userTickets, remainingTickets)

	if isValidEmail && isValidName && isValidTicketNumber {

		bookTickets(userTickets, firstName, lastName, email)

		wg.Add(1)
		go sendTicket(userTickets, firstName, lastName, email)

		firstName := getFirstName()
		fmt.Printf("The first name of booking are: %v\n", firstName)

		if remainingTickets == 0 {
			fmt.Println("Our conference is booked out! Please come next year")
		}

	} else {
		if !isValidEmail {
			fmt.Println("You have eneterd invalid email")
		}
		if !isValidName {
			fmt.Println("Ypu have enterd invalid name")
		}
		if !isValidTicketNumber {
			fmt.Println("You have enterd invalid ticket number")
		}
		fmt.Println("Please enter the details again.")
	}

	wg.Wait()

}

func greetUsers() {
	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available.\n", conferenceTickets, remainingTickets)

	fmt.Println("Get your tickets here to attend")
}

func getFirstName() []string {
	firstNames := []string{}
	for _, booking := range bookings {
		firstNames = append(firstNames, booking.firstName)
	}

	return firstNames
}

func getUserDetails() (string, string, string, int) {
	var firstName string
	var lastName string
	var email string
	var userTickets int

	fmt.Println("Enter your  first name ")
	fmt.Scan(&firstName)

	fmt.Println("Enter your last name: ")
	fmt.Scan(&lastName)

	fmt.Println("Enter your Email: ")
	fmt.Scan(&email)

	fmt.Println("Enter number of tickets: ")
	fmt.Scan(&userTickets)

	return firstName, lastName, email, userTickets
}

func bookTickets(userTickets int, firstName string, lastName string, email string) {
	remainingTickets -= userTickets

	//Creating a struct for a user
	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTickets,
	}

	bookings = append(bookings, userData)
	fmt.Printf("List of bookings is %v\n", bookings)

	fmt.Printf("Thank you %v  %v for booking %v tickets.You will receive a confirmation email at %v\n", firstName, lastName, userTickets, email)
	fmt.Printf("%v tickets are remaing for %v\n", remainingTickets, conferenceName)

}

func sendTicket(userTickets int, firstname string, lastName string, email string) {
	time.Sleep(10 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTickets, firstname, lastName)
	fmt.Println("#######################")
	fmt.Printf("Sending tickets: \n %v \nto email address %v\n", ticket, email)
	fmt.Println("#######################")
	wg.Done()
}
